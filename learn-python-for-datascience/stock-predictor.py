import pandas as pd
import numpy as np
from sklearn.svm import SVR
import matplotlib.pyplot as plt

# plt.switch_backend()

df = pd.read_csv('src/aapl.csv')

dates = [date.split('-')[0] for date in df['Date'].to_numpy()]
prices = df['Open'].to_numpy()

def predict_prices(dates, prices, x):
    dates = np.reshape(dates, (len(dates), 1))
    svr_lin = SVR(kernel='linear', C=1e3, gamma='scale')
    svr_poly = SVR(kernel='poly', C=1e3, degree=2, gamma='scale')
    svr_rbf = SVR(kernel='rbf', C=1e3, gamma=0.1)
    print('hello')
    svr_lin.fit(dates, prices)
    svr_poly.fit(dates, prices)
    svr_rbf.fit(dates, prices)
    print('hello')
    plt.scatter(dates, prices, color='black', label='Data')
    plt.plot(dates, svr_rbf.predict(dates), color='red', label='RBF Model')
    plt.plot(dates, svr_poly.predict(dates), color='blue', label='Poly Model')
    plt.plot(dates, svr_lin.predict(dates), color='black', label='Linear Model')
    plt.xlabel('Date')
    plt.ylabel('Price')
    plt.title('Support Vector Regression')
    plt.legend()
    plt.show()

    return svr_lin.predict(x)[0], svr_poly.predict(x)[0], svr_rbf.predict(x)[0]

print(predict_prices(dates, prices, 29))

 