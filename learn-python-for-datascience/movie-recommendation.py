import numpy as np
from lightfm.datasets import fetch_movielens
from lightfm import LightFM

# Collaborative Based => Predict what I like based on what others have liked
# Content Based => Predict what I like based on what I have liked in the past

# fetch data and format it
data = fetch_movielens(min_rating=4.0)

# print training and testing data
print(repr(data['train']))
print(repr(data['test']))
print(data['item_labels'], len(data['item_labels']))

# create model (Content + Collaborative = Hybrid)
model = LightFM(loss='warp') # Weighted Approximate-Rank Pairwise (Gradient Descent)

model.fit(data['train'], epochs=30, num_threads=2)

def sample_recommendation(model: LightFM, data, user_ids):
    # number of users and movies in training data
    n_users, n_items = data['train'].shape
    
    # generate recommendations for each user we input
    for uid in user_ids:
       # movies they already like
       #    tocsr() turns a matrix into a compressed sparse row (aquivalent of a list)
       known_positives = data['item_labels'][data['train'].tocsr()[uid].indices]

       # movies our model predicts
       scores = model.predict(uid, np.arange(n_items))
       # rank them from most to least liked
       top_items = data['item_labels'][np.argsort(-scores)]
    
       print(f"User #{uid}")
       print("    Known positives:")
       for movie in known_positives[:3]:
           print(f"        {movie}")
       print("    Recommended:")
       for movie in top_items[:3]:
           print(f"        {movie}")

sample_recommendation(model, data, [3, 25, 450])
