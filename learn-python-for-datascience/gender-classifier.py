from sklearn import tree
from sklearn.svm import SVC
from sklearn.naive_bayes import GaussianNB

# Decision Path
# A bunch of yes / no paths that result in a label at the end of the tree 

# [height, weight, shoe size]
X = [[181, 80, 44], [177, 70, 43], [160, 60, 38], [154, 54, 37], 
     [166, 65, 40], [190, 90, 47], [175, 64, 39], [177, 70, 40],
     [159, 55, 37], [171, 75, 42], [181, 85, 43]]


Y = ["male", "male", "female", "female", "male", "male", "male",
     "female", "male", "female", "male"]

predictions = []
sample = [[190, 70, 43]]
classifiers = [tree.DecisionTreeClassifier(), SVC(kernel="linear"), GaussianNB()]

for classifier in classifiers:
    classifier = tree.DecisionTreeClassifier()
    model = classifier.fit(X, Y)
    predictions.append(model.predict(sample)[0])

print(predictions)
